8console.log('activated');

var count = parseInt(document.getElementById('count').value);
var form = document.getElementById('forms');

var addForm = function(){
    var newPart = document.createElement('div');
    count += 1;
    document.getElementById('count').value = count;
    newPart.innerHTML = newForm(count);
    form.appendChild(newPart);
}

var removeForm = function(){
    count -= 1;
    document.getElementById('count').value = count;
    form.removeChild(form.lastChild);
}

var newForm = function(id){
    var code = "\
    <div class='card'>\
    <div class='card-header'>\
        Entry "+id+"\
    </div>\
    <div class='card-body'>\
    <div class='form-row'> \
        <div class='form-group col-md-8'> \
            <label for='inputEmail4'>Name</label> \
            <input name='name"+id+"' type='text' class='form-control' id='inputEmail4' placeholder='Email' required> \
        </div> \
        <div class='form-group col-md-8'> \
            <label for='inputPassword4'>File</label> \
            <input name='file"+id+"' type='file' class='form-control' id='inputPassword4' placeholder='Password' onChange='displayImage(this,"+id+")' required> \
        </div> \
        <img id='image"+id+"' class='mr-3'  alt='Generic placeholder image'>\
        <div class='form-group col-md-8'> \
            <div class='form-check'> \
                <input name='check"+id+"' class='form-check-input' type='checkbox' id='gridCheck'> \
                <label class='form-check-label' for='gridCheck'> \
                    Check \
                </label> \
            </div> \
            <div class='form-check'> \
                <input name='repair"+id+"' class='form-check-input' type='checkbox' id='gridCheck'> \
                <label class='form-check-label' for='gridCheck'> \
                    Repair \
                </label> \
            </div> \
        </div> \
    </div></div></div><br/>";
    return code;
}

function displayImage(input,id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById('image'+id).setAttribute('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}