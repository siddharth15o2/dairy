<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy"
        crossorigin="anonymous">

    <title>Dairy</title>
</head>

<body style='background:#f4f4f4'>
    <br/>
    <br/>
    <div class='container'>
        <div>
            <div class='d-flex justify-content-between'>
                <div>
                    <h1 style='display:inline'>Dairy</h1>&nbsp;&nbsp;&nbsp;
                    <a class='btn btn-primary' onClick='addForm();'>Add</a>
                </div>
                <div>
                    <a class='btn btn-primary' onClick='removeForm();'>Remove</a>
                </div>
            </div>
            <br/>
            <div>
                <form action='serverx.php' method='POST' enctype="multipart/form-data">
                    <input hidden name='count' value='1' id='count'>
                    <div id='forms'>
                        <div>
                            <div class='card'>
                                <div class='card-header'>
                                    Entry 1
                                </div>
                                <div class='card-body'>
                                    <div class="form-row">
                                        <div class="form-group col-md-8">
                                            <label for="inputEmail4">Name</label>
                                            <input name='name1' type="text" class="form-control" id="inputEmail4" placeholder="Email" required>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <label for="inputPassword4">File</label>
                                            <input name='file1' type="file" class="form-control" id="inputPassword4" onChange='displayImage(this,1)' required>
                                        </div>
                                        <img id='image1' class="mr-3" src="..." alt="Generic placeholder image"  >
                                        <div class="form-group col-md-8">
                                            <div class="form-check">
                                                <input name='check1' class="form-check-input" type="checkbox" id="gridCheck">
                                                <label class="form-check-label" for="gridCheck">
                                                    Check
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input name='repair1' class="form-check-input" type="checkbox" id="gridCheck">
                                                <label class="form-check-label" for="gridCheck">
                                                    Repair
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
        crossorigin="anonymous"></script>

    <!-- Project Script             -->
    <script src="js/index.js"></script>
</body>

</html>
<?php 

?>